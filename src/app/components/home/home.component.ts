import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title: string
  description: string;

  constructor() {
    this.title = "Comic App";
    this.description = "Esta es una aplicacion de comics."
  }

  ngOnInit(): void {
  }

}
